json.array!(@items) do |event|
  json.extract! event, :type, :title
  json.url url_for(event)
end