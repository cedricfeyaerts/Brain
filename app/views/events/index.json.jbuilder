json.array!(@items) do |event|
  json.extract! event, :id, :title, :description, :start, :end
  json.allDay event.full_day
  json.url url_for(event)
end