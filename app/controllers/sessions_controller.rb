class SessionsController < ApplicationController
  require 'oauth2'

  require 'onedrive'

  def onedrive_callback
    
    sign_in User.last

    code = params['code']
    client = OAuth2::Client.new(ENV['ONEDRIVE_LOGIN'] , ENV['ONEDRIVE_PWD'] , :site => 'https://login.live.com' , :authorize_url => '/oauth20_authorize.srf' , :token_url => '/oauth20_token.srf')
    token = client.auth_code.get_token(code, :redirect_uri => "https://cedric-is-back.com/auth/onedrive/callback")
    if token
      token_hash = token.to_hash
      if token_hash[:refresh_token]
        @token = User.last.tokens.create(provider: 'onedrive', 
          token: token_hash[:access_token],
          expires_at: token_hash[:expires_at],
          refresh_token: token_hash[:refresh_token])
      end
      
    end

    return render json: token

    
    client = Onedrive::Client.new token: token.token
    

    render json: client.drives.first.children
    # render json: params


  end

  # def new

  #   client = OAuth2::Client.new('718706077076-hbolvo936lur8qamum1sipfbgr2hunms.apps.googleusercontent.com', 'xA1Ev32x-zocAX7sxhtcqp3t', :site => 'https://accounts.google.com',:authorize_url => '/o/oauth2/v2/auth', :token_url     => '/o/oauth2/token')

  #   Rails.application.config.oauth2_client = client
  #   url = client.auth_code.authorize_url(:redirect_uri => 'http://vagrant2.com:3000/auth/google_oauth2/callback',
  #     scope:'https://www.google.com/m8/feeds', access_type:'offline')

  #   redirect_to url

  # end


  def onedrive_login

    # https://login.live.com/oauth20_desktop.srf

    client = OAuth2::Client.new(ENV['ONEDRIVE_LOGIN'] , ENV['ONEDRIVE_PWD'] , :site => 'https://login.live.com' , :authorize_url => '/oauth20_authorize.srf' , :token_url => '/oauth20_token.srf')
    # client = OAuth2::Client.new(onedrive_login , onedrive_pwd , :site => 'http://192.168.33.11:3000' , :authorize_url => '/auth/test' , :token_url => '/auth/test')

    url = client.auth_code.authorize_url(:redirect_uri => 'https://cedric-is-back.com/auth/onedrive/callback',
      scope:'onedrive.readwrite offline_access', response_type:'code')

    p '----------------------'

    # token = client.auth_code.get_token('my_code', :redirect_uri => 'https://cedric-is-back.com/users/auth/google_oauth2/callback')
    
    p '------------------'
    # return render text: token
    redirect_to url

  end

  def failure

  end

  # def test
  #   p '==========='
  #   p params
  #   p '=========='
  #   render text: 'ok'


  #   {"client_id"=>"42e51fca-0194-4603-b4e1-4b0665b5ca70", 
  #     "client_secret"=>"MHAZPSZQzhw1Y0hgDD0iQVa",
  #      "code"=>"M1a6bb514-110d-eafc-920c-59b1d634a118", 
  #      "grant_type"=>"authorization_code", 
  #      "redirect_uri"=>"https://cedric-is-back.com/users/auth/google_oauth2/callback"}

  #   query = { 
  #     "method" => "neworder",
  #     "nonce" => 1404996028,
  #     "order_type" => "buy",
  #     "quantity" => 1,
  #     "rate" => 1
  #   }
  #   headers = {"client_id"=>"42e51fca-0194-4603-b4e1-4b0665b5ca70", 
  #     "client_secret"=>"MHAZPSZQzhw1Y0hgDD0iQVa",
  #      "code"=>"M1a6bb514-110d-eafc-920c-59b1d634a118", 
  #      "grant_type"=>"authorization_code", 
  #      "redirect_uri"=>"https://cedric-is-back.com/users/auth/google_oauth2/callback"}

  #   tata = HTTParty.post(
  #     "https://www.acb.com/api/v2/market/LTC_BTC/", 
      
  #     :headers => headers
  #   )

  #   render json: tata
  # end

  

end
