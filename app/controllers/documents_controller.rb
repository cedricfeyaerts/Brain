class DocumentsController < ItemsController

  protected

  def item_params
    params.require(:document).permit(:title, :pre, :content, related_items_attributes: [:id, :title, :related_item_id, :position, :_destroy], infos_attributes: [:id, :title, :content, :ref, :_destroy])
  end

  def items_scope
    current_user.items.documents
  end

end
