class ItemsController < ApplicationController

  before_action :authenticate_user!, :get_class

  def index
    @q = items_scope.order(created_at: 'desc').ransack(params[:q])
    @items = @q.result(distinct: true).page(params[:page])

  end

  def new
    @item = @klass.new
  end

  def create
    @item = @klass.new(params_with_tag)
    # return render json: @item.tags
    @item.user_id = current_user.id
    flash[:info] = "'#{@item.title}' created" if @item.save

    yield @item if block_given?

    respond_with @item, location: url_for(@klass)
  end

  def edit
    @item = items_scope.find(params[:id])
  end

  def update
    # return render json: tags_params
    @item = items_scope.find(params[:id])
    flash[:info] = "'#{@item.title}' updated" if @item.update(params_with_tag)

    yield @item if block_given?

    respond_with @item, location: url_for(@klass)
  end

  def show
    @item = items_scope.find(params[:id])
  end

  def destroy
    item = items_scope.find(params[:id])
    item.destroy()
    flash[:info] = "'#{item.title}' deleted"
    respond_with item, location: url_for(@klass)
  end
 

  protected

  def item_params
    params.require(@sym).permit(:title, :pre, :content, related_items_attributes: [:id, :title, :related_item_id, :position, :_destroy], infos_attributes: [:id, :title, :content, :ref, :_destroy])
  end

  def tags_params
    params.require(@sym).permit(tags:[])
  end

  def params_with_tag
    prms = item_params.as_json
    prms[:tag_string] = tags_params[:tags].compact.uniq.join(' ') unless tags_params[:tags].blank?
    prms
  end

  def items_scope
    current_user.items
  end

  def get_class
    @sym = controller_name.singularize.to_sym
    @klass = controller_name.singularize.camelize.constantize
  end



end

