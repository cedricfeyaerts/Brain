class TodoListsController < ItemsController

  protected

  def item_params
    params.require(:todo_list).permit(:title, :pre,:start, :end, :content, related_items_attributes: [:id, :title, :related_item_id, :position, :_destroy], infos_attributes: [:id, :title, :content, :ref, :_destroy], todos_attributes: [:id, :title, :done_boolean, :position, :_destroy])
  end

  def items_scope
    current_user.items.todo_lists
  end


end
