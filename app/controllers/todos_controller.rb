class TodosController < ApplicationController

  before_action :authenticate_user!

  def create
    @todo = Todo.new(todo_params)
    check_permission
    @todo.save

    respond_with @todo#, location: edit_todo_list_path(@todo_list)
  end


  def update
    @todo = Todo.find(params[:id])
    check_permission
    @todo.update(todo_params)

    respond_with @todo#, location: edit_todo_list_path(@todo_list)
  end


  def destroy
    @todo = Todo.find(params[:id])
    check_permission
    @todo.destroy()

    respond_with @todo#, location: edit_todo_list_path(@todo_list)
  end

  
 

  private

  def todo_params
    params.require(:todo).permit(:id, :title, :done_boolean, :item_id)
  end

  def check_permission
    raise 'not permitted' unless @todo.item.user == current_user
  end



end
