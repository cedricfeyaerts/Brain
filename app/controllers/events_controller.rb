class EventsController < ItemsController
  require 'apis'

  before_action :authenticate_user!

  def index
    @items = current_user.items.all.where.not('start' => nil)
    @items = @items.where('start >= ?',params[:start]) unless params[:start].blank?
    @items = @items.where('start <= ?',params[:end]) unless params[:end].blank?
  end


  def create
    super do |event|

      api = Apis::GoogleCalendar.new current_user.tokens.refresheable.provider('google_oauth2').last
      api.create_event(event)

    end
  end


  def update
    super do |event|

      api = Apis::GoogleCalendar.new current_user.tokens.refresheable.provider('google_oauth2').last
      api.update_event(event)

    end
  end

  protected

  def item_params
    params.require(:event).permit(:title, :pre, :content, :start, :end, related_items_attributes: [:id, :title, :related_item_id, :position, :_destroy], infos_attributes: [:id, :title, :content, :ref, :_destroy])
  end

  def items_scope
    current_user.items.events
  end

end
