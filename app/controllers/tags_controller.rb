class TagsController < ApplicationController

  before_action :authenticate_user!

  def index
    @tags = Tag.all.limit(5)
    render json: @tags.map(&:title).as_json
  end

  def suggest

    @tags = Tag.where("title LIKE ?", "%#{params[:query]}%").limit(5)
    render json: {results: @tags.map{|t|{id:t.title, text:t.title}}.as_json }

  end

end
