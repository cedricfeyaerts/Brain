class PagesController < ApplicationController

  before_action :authenticate_user!

  def home
    @q = Item.ransack(params[:q])
    @items = @q.result(distinct: true)
  end

end