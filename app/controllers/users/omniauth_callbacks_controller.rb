class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def google_oauth2
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      if request.env["omniauth.auth"]["credentials"]["refresh_token"]
        @token = @user.tokens.create(provider: 'google_oauth2', 
          token: request.env["omniauth.auth"]["credentials"]["token"],
          expires_at: 1.hour.from_now,
          refresh_token: request.env["omniauth.auth"]["credentials"]["refresh_token"]) 
      end
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Google"
      sign_in_and_redirect @user, :event => :authentication
    else
      session["devise.google_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

  def failure
    p '-------------------'
    p failure_message
    render json: failure_message
  end
end