module ApplicationHelper
  require 'kramdown'
  
  def markdown(text)
    return '' if text.blank?
    options = {
      filter_html:     true,
      hard_wrap:       true, 
      link_attributes: { rel: 'nofollow' },
      space_after_headers: true, 
      fenced_code_blocks: true,
      with_toc_data: true

    }

    extensions = {
      lax_spacing:true,
      autolink:true,
      strikethrough: true,
      underline: true,
      highlight: true,
      quote: true,
      footnotes: true,
      superscript:        true,
      disable_indented_code_blocks: true,
      tables: true
    }

    renderer = Redcarpet::Render::HTML.new( options)
    markdown = Redcarpet::Markdown.new(renderer, extensions)

    markdown.render(text).html_safe
  end

  def markdown2(text)
    Kramdown::Document.new(text, input: 'GFM').to_html
  end

  def downgrade md
    md.gsub(/(?<foo>#+)/, '#\k<foo>')
  end

  def full_content(article)
    c = article.content#.gsub(/(?<foo>#+)/, '#\k<foo>')
    relateds = article.related_items.map do |related_item|
      next unless related_item.related_item.is_a? Article
      title = related_item.title.blank? ? related_item.related_item.title : related_item.title
      c += "\n# #{title} \[[link]\](#{url_for(related_item.related_item)})\n"
      c += full_content(related_item.related_item)
    end
    downgrade c
  end

  def add_toc(md)
    "\n* TOC\n{:toc}\n#{md}"
  end












end
