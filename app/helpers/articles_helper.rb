module ArticlesHelper

  def autolinks(html)
    links = html.scan(/\[\[([^\[\]<>]+)\]\]/).flatten.map(&:downcase)

    definitions = Hash[Item.where('lower(title) IN (?)', links).map{|i| [i.title, i.pre]}]

    definitions.each do |title, definition|
      html = html.gsub(/\[\[#{title}\]\]/i, content_tag('strong', title, 'data-toggle':"text-primary" , title:definition, class:'text-info tooltiped' ))
    end
    html
  end

end
