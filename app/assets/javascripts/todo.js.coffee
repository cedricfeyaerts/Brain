class Todo

  constructor:(@$container)->

  init:()->
    # @$container = $('.todos')
    @last = @$container.find('.list-group-item').last()
    @$input = @$container.find("input.new_todo")
    @url = @$input.data('url')
    @item_id = @$input.data('item-id')
    @$input.on('keypress', (e)=>
      if e.which == 13
        @create(@$input.val())
        @$input.val('')
        
    )
    @$container.on('click', '.todo .close', @destroy)
    @$container.on('click', '.todo', @toggle)

  toggle:(e)=>
    console.log  $(e.currentTarget, e.target)
    $todo = $(e.currentTarget)
    if $todo.hasClass('done')
      data = {todo:{done_boolean:'0', item_id:@item_id}}
    else
      data = {todo:{done_boolean:'1', item_id:@item_id}}

    $todo.toggleClass('done')

    $.ajax(
      type: "PUT",
      url: "#{@url}/#{$todo.data('id')}",
      data: data,
      success: (data)->

      dataType: 'json'
    )

  destroy:(e)=>
    e.stopPropagation()
    console.log  $(e.currentTarget)
    $todo = $(e.currentTarget).closest('.todo')
    $todo.remove()
    

    $.ajax(
      type: "DELETE",
      url: "#{@url}/#{$todo.data('id')}",
      data: {},
      success: (data)->

      dataType: 'json'
    )

      
    
  
  create:(val)->

    $todo = $("<div class='list-group-item todo'><strong>#{val}</strong><button class=\"close text-muted pull-right\"><span>×</span></button></div>")

    @last.before($todo)
    data = {todo:{title:val, item_id:@item_id}}

    $.ajax(
      type: "POST",
      url: @url,
      data: data,
      success: (data)->
        $todo.attr('data-id', data.id)

      dataType: 'json'
    )

    





ready = -> 
  
  $(".todos").each (i, el)->
    todo = new Todo($(el))
    todo.init()



$(document).on('turbolinks:load', ready)