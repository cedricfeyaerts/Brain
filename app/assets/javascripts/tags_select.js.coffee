ready = ->
  $(".tags_select").select2({
    theme: "bootstrap";
    tags: true,
    tokenSeparators: [',', ' '],
    ajax: {
      url: (params)->
        console.log '/tags/suggest/'+params.term
        return '/tags/suggest/'+params.term
      ,
      delay: 250
    }
  })

$(document).on('turbolinks:load', ready)