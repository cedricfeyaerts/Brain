ready = -> 

  $('.datetimepicker').datetimepicker()

  $('[data-toggle="tooltip"]').tooltip()

  $( ".select2" ).select2({
    theme: "bootstrap"
  })

  
  $( ".sortable" ).sortable({
    placeholder: "ui-state-highlight",
    items: ".row:not(.no-sort)",
    update: (e,ui)->
      ui.item.closest( ".sortable" ).find('input.sort_position').each (i,el)->
        $(el).val(i)
    })
  $( ".sortable" ).disableSelection();


  $('.cocoon_container').on('cocoon:after-insert', (e, insertedItem) ->
    $( ".sortable" ).sortable( "refresh" )
    $( ".select2" ).select2({
      theme: "bootstrap"
    })
  )
  $('.cocoon_container').on('cocoon:after-remove', (e, insertedItem) ->
    $( ".sortable" ).sortable( "refresh" )
  )
  

  $('#calendar').empty()
  $('#calendar').fullCalendar(
    events: '/events.json',
    eventRender: (event, element) ->
      element.tooltip('title':event.description)
    ,
    eventDrop: (event, delta, revertFunc) ->
      console.log event,  event.start.format()
      $.ajax({
        type: "PUT",
        url: "/events/#{event.id}",
        data: { 'event[start]': event.start.format() },
        success: (data)->
          console.log data,
        dataType: 'json'
        
      })
    ,
    selectable: true,
    selectHelper: true,
    select: (start, end) ->
      title = prompt('Event Title:');
      if (title) 
        eventData = {
          title: title,
          start: start,
          end: end
        }
        $('#calendar').fullCalendar('renderEvent', eventData, true); 
        $.ajax({
          type: "POST",
          url: "/events",
          data: { 'event[start]': eventData.start.format(),'event[end]': eventData.end.format(),'event[title]':title },
          success: (data)->
            console.log data,
          dataType: 'json'
          
        })
      
      $('#calendar').fullCalendar('unselect');
    ,
    editable: true,

      
  )


  items = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: '/items.json',
    remote: {
      url: '/items.json?q[title_cont]=%QUERY',
      wildcard: '%QUERY'
    }
  })

  $('.typeahead').typeahead(null, {
    name: 'Items',
    display: 'title',
    source: items,
    templates: {
      empty: [
        '<div class="empty-message">',
          'unable to find any Best Picture winners that match the current query',
        '</div>'
      ].join('\n'),
      suggestion: (obj)->
        "<div><strong>#{obj.title}</strong> – #{obj.type}</div>"
    }
  });

  $('.typeahead').bind('typeahead:select', (ev, suggestion) ->
    console.log(suggestion)
    document.location.href = suggestion.url
  )



$(document).on('turbolinks:load', ready)