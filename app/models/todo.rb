class Todo < ActiveRecord::Base
  has_ancestry

  belongs_to :item

  default_scope {order(position: 'asc')}
  validates :title, presence:true

  def done_boolean
    !self.done.blank?
  end
  def done_boolean=val
    if val == '1'
      self.done = Time.now
    else
      self.done = nil
    end
  end
  # alias_method  :todo_list_id, :item_id
  # alias_method  :todo_list_id=, :item_id=

  
end
