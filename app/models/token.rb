class Token < ActiveRecord::Base
  belongs_to :user

  scope :refresheable, ->{where.not(refresh_token: nil)}
  scope :active, ->{where.not('expires_at > ?': Time.now)}
  scope :provider, ->(provider){where(provider: provider)}
  default_scope {order(updated_at: 'asc')}

  def to_token
    case provider
    when 'google_oauth2'
      client = OAuth2::Client.new(ENV['GOOGLE_CLIENT_ID'],ENV['GOOGLE_CLIENT_SECRET'],:site => 'https://accounts.google.com',:authorize_url => '/o/oauth2/v2/auth', :token_url     => '/o/oauth2/token')

      # '//www.googleapis.com/oauth2/v4/token'

    when 'onedrive'
      client = OAuth2::Client.new(ENV['ONEDRIVE_LOGIN'] , ENV['ONEDRIVE_PWD'] , :site => 'https://login.live.com' , :authorize_url => '/oauth20_authorize.srf' , :token_url => '/oauth20_token.srf')
    end
    return nil unless client
    OAuth2::AccessToken.from_hash(client, :access_token => token, :refresh_token => refresh_token, :expires_at => expires_at)
  end

  def to_authorizer(scope)
    case provider
    when 'google_oauth2'
      authorizer = Signet::OAuth2::Client.new(
      :authorization_uri     => 'https://accounts.google.com/o/oauth2/auth',
      :token_credential_uri  => 'https://accounts.google.com/o/oauth2/token',
      :client_id             => ENV['GOOGLE_CLIENT_ID'],
      :client_secret         => ENV['GOOGLE_CLIENT_SECRET'],
      access_token:         token,
      refresh_token:        refresh_token,
      scope:                scope)

    end
    authorizer
  end

  def from_token_hash(token_hash)
    p token_hash
    p token_hash[:expires_at]
    update(token: token_hash[:access_token], expires_at: Time.at(token_hash[:expires_at]))
    p expires_at
  end


  def refresh
    token_hash = to_token.refresh!.to_hash
    from_token_hash(token_hash)
    self
  end

  def is_expired?
    return true if expires_at.nil?
    expires_at < Time.now
  end

  def refresh_if_expired
    self.refresh if is_expired?
    self
  end

  def apply!(headers)
    headers['Authorization'] = "Bearer #{token}"
  end

end