class Definition < Item
  require 'wikipedia'
  require 'open-uri'
  require 'json'

  def self.pull_from_wiki(title)
    page = Wikipedia.find( title )
    unless page.content
      p '---------------'
      title_wiki = search_wiki(title)
      p title_wiki
      return unless title_wiki
      p 'ok'
      page = Wikipedia.find( title_wiki )
    end
    summary = (page.summary rescue '')
    self.create title: title, pre: summary
  end


  def self.search_wiki(title)
    title = ERB::Util.url_encode(title)
    uri = "https://fr.wikipedia.org/w/api.php?action=query&list=search&srsearch=#{title}&format=json&utf8="
    result = JSON.parse(open(uri).read)
    result["query"]["search"][0]["title"] rescue nil
  end
end
