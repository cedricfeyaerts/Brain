class Article < Item

  after_save :create_missing_definitions



  def scan_autolink
    self.content.scan(/\[\[([^\[\]<>]+)\]\]/).flatten.map(&:downcase)
  end

  def create_missing_definitions
    autolinks = scan_autolink
    titles = Item.where('lower(title) IN (?)', autolinks).map(&:title)
    (autolinks-titles).each do |title|
      p title
      Definition.pull_from_wiki(title)
    end
  end



end
