class Item < ActiveRecord::Base

  belongs_to :user
  has_and_belongs_to_many :tags
  has_many :infos, dependent: :destroy
  has_many :todos, dependent: :destroy
  has_many :related_items, dependent: :destroy
  has_many :items, :through => :related_items
  has_many :inverse_related_items, :class_name => "RelatedItem", :foreign_key => "related_item_id", dependent: :destroy
  has_many :inverse_items, :through => :inverse_related_items, :source => :item

  accepts_nested_attributes_for :tags
  accepts_nested_attributes_for :todos
  accepts_nested_attributes_for :infos
  accepts_nested_attributes_for :related_items, reject_if: :all_blank, allow_destroy: true

  scope :articles, ->{where(type: "Article")}
  scope :documents, ->{where(type: "Document")}
  scope :contacts, ->{where(type: "Contact")}
  scope :definitions, ->{where(type: "Definition")}
  scope :events, ->{where(type: "Event")}
  scope :todo_lists, ->{where(type: "TodoList")}

  def tag_string
    self.tags.map(&:title).join(' ')
  end

  def tag_string=(string)
    t = Tag.create_from_string(string)
    self.tags = t.uniq
  end

end


class RelatedItem < ActiveRecord::Base
  belongs_to :item
  belongs_to :related_item, :class_name => "Item"
  default_scope {order(position: 'asc')}
end