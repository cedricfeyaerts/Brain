class Event < Item

  validates :start, presence: true

  def full_day
    self.end.blank? or self.start == self.start.midnight()
  end

  def full_day=(val)
  end

  def description
    return self.pre unless pre.blank?
    return self.content
  end

  def link=value
    self.infos.where(ref: 'link').first_or_create.tap do  |info|
      info.update content: value,
        title: 'link'
    end
  end
  
  def calendar_id=value
    self.infos.where(ref: 'calendar_id').first_or_create.tap do  |info|
      info.update content: value,
        title: 'calendar_id'
    end
  end

end
