class TodoList < Item

  # has_many :todos

  # accepts_nested_attributes_for :todos

  def full_day
    self.end.blank? or self.start == self.start.midnight()
  end

  def full_day=(val)
  end

  def description
    return self.pre unless pre.blank?
    return self.content
  end
  
end
