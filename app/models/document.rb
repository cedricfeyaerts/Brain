class Document < Item

  before_save :set_tags


  def url=value
    self.infos.where(ref: 'url').first_or_create.tap do  |info|
      info.update content: value,
        title: 'url'
    end
  end

  def path=value
    self.infos.where(ref: 'path').first_or_create.tap do  |info|
      info.update content: CGI::unescape(value),
        title: 'path'
    end
  end

  def link=value
    self.infos.where(ref: 'link').first_or_create.tap do  |info|
      info.update content: value,
        title: 'link'
    end
  end

  def set_tags
    path = self.infos.where(ref: 'path').first
    return unless path
    t = CGI::unescape(path.content).split('/').reject{ |c| c.empty? }.map{|x|x.parameterize('_')}.join(' ')
    self.tag_string = t

  end




end
