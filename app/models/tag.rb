class Tag < ActiveRecord::Base
  has_and_belongs_to_many :articles

  def self.create_from_string(string)
    p string
    words = string.downcase.lstrip.split(/\W+/)
    words.map do |word|
      p '----------'
      p "->#{word}<-"
      self.find_or_create_by(title: word)
    end
  end


end
