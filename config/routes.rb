Rails.application.routes.draw do
  devise_for :users, :controllers => { 
    :omniauth_callbacks => "users/omniauth_callbacks",
    registrations: 'users/registrations'

     }
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  resources :articles
  resources :documents
  resources :definitions
  resources :events
  resources :contacts
  resources :todo_lists
  resources :todos, only:[:create,:update,:destroy]
  
  resources :tags, only: [:index] do
    collection do 
      get "suggest(/:query)", as: :suggest, action: :suggest
    end
  end

  get  '/login', :to => 'sessions#new', :as => :login
  get  '/onedrive', :to => 'sessions#onedrive_login', :as => :onedrive
  get '/auth/onedrive/callback', to: 'sessions#onedrive_callback'
  get '/auth/failure', :to => 'sessions#failure'
  get '/auth/test', :to => 'sessions#test'
  post '/auth/test', :to => 'sessions#test'
  get '/post', :to => 'sessions#post'

  get :items, to: 'items#index'

  
  root 'pages#home'
end
