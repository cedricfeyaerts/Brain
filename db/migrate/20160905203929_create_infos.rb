class CreateInfos < ActiveRecord::Migration
  def change
    create_table :infos do |t|
      t.string :title
      t.string :content
      t.string :ref
      t.belongs_to :item, index: true
      t.timestamps null: false
    end
  end
end
