class CreateDefinitions < ActiveRecord::Migration
  def change
    rename_table :articles, :items

    add_column :items, :type, :string, :limit => 100, :null => false, :default => "Article"
  end
end
