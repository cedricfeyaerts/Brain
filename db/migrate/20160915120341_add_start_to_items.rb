class AddStartToItems < ActiveRecord::Migration
  def change
    add_column :items, :start, :datetime
    add_column :items, :end, :datetime
  end
end
