class CreateRelatedItems < ActiveRecord::Migration
  def change
    create_table :related_items do |t|
      t.string :title
      t.integer  "item_id", index: true
      t.integer  "related_item_id", index: true
    end
  end
end
