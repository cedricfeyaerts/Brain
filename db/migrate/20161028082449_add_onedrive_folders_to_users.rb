class AddOnedriveFoldersToUsers < ActiveRecord::Migration
  def change
    add_column :users, :onedrive_folders, :text, array: true, default: []
  end
end
