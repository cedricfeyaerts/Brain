class AddPositionToTodo < ActiveRecord::Migration
  def change
    add_column :todos, :position, :integer
    add_column :related_items, :position, :integer
  end
end
