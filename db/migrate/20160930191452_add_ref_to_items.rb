class AddRefToItems < ActiveRecord::Migration
  def change
    add_column :items, :ref, :string, index: true
    add_index :items, :ref
  end
end
