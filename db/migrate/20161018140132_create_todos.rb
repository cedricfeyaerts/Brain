class CreateTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      t.references :item, index: true
      t.string :title
      t.datetime :done
      t.string :ancestry, index:true
      

      t.timestamps null: false
    end
  end
end
