class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :title

      t.timestamps null: false
    end

    create_table :items_tags, id: false do |t|
      t.belongs_to :item, index: true
      t.belongs_to :tag, index: true
    end
  end
end
