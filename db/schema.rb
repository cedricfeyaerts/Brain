# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161028143527) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "infos", force: :cascade do |t|
    t.string   "title"
    t.string   "content"
    t.string   "ref"
    t.integer  "item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "infos", ["item_id"], name: "index_infos_on_item_id", using: :btree

  create_table "items", force: :cascade do |t|
    t.string   "title"
    t.text     "pre"
    t.text     "content"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "type",       limit: 100, default: "Article", null: false
    t.datetime "start"
    t.datetime "end"
    t.string   "ref"
    t.integer  "user_id"
  end

  add_index "items", ["ref"], name: "index_items_on_ref", using: :btree
  add_index "items", ["user_id"], name: "index_items_on_user_id", using: :btree

  create_table "items_tags", id: false, force: :cascade do |t|
    t.integer "item_id"
    t.integer "tag_id"
  end

  add_index "items_tags", ["item_id"], name: "index_items_tags_on_item_id", using: :btree
  add_index "items_tags", ["tag_id"], name: "index_items_tags_on_tag_id", using: :btree

  create_table "related_items", force: :cascade do |t|
    t.string  "title"
    t.integer "item_id"
    t.integer "related_item_id"
    t.integer "position"
  end

  add_index "related_items", ["item_id"], name: "index_related_items_on_item_id", using: :btree
  add_index "related_items", ["related_item_id"], name: "index_related_items_on_related_item_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "todos", force: :cascade do |t|
    t.integer  "item_id"
    t.string   "title"
    t.datetime "done"
    t.string   "ancestry"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "position"
  end

  add_index "todos", ["ancestry"], name: "index_todos_on_ancestry", using: :btree
  add_index "todos", ["item_id"], name: "index_todos_on_item_id", using: :btree

  create_table "tokens", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "token"
    t.string   "refresh_token"
    t.datetime "expires_at"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "tokens", ["user_id"], name: "index_tokens_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "provider"
    t.string   "uid"
    t.string   "image"
    t.text     "onedrive_folders",       default: [],              array: true
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "items", "users"
end
