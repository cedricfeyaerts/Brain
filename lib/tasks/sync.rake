require 'apis'
namespace :sync do

  desc "sync all documents"
  task :documents => :environment do
    User.all.each do |user|
      token = user.tokens.refresheable.provider('onedrive').last
      return unless token
      api = Apis::Onedrive.new(token)
      api.sync_documents
    end
  end

  desc "sync all contacts"
  task :contacts => :environment do
    User.all.each do |user|
      token = user.tokens.refresheable.provider('google_oauth2').last
      return unless token
      api = Apis::GoogleContact.new(token)
      api.sync_contacts
    end
  end

  desc "sync all events"
  task :events => :environment do
    User.all.each do |user|
      token = user.tokens.refresheable.provider('google_oauth2').last
      return unless token
      api = Apis::GoogleCalendar.new(token)
      api.sync_events
    end
  end

  

end