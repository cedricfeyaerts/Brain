class Apis::GoogleDrive
  require 'google/apis/drive_v3'

  # AUTHORIZATION_URI = 'https://accounts.google.com/o/oauth2/auth'
  # TOKEN_CREDENTIAL_URI = 'https://accounts.google.com/o/oauth2/token'
  attr_accessor :events
  attr_accessor :token
  attr_accessor :client

  def initialize(token)
    @token = token

    @token.refresh_if_expired
    scope = Google::Apis::DriveV3::AUTH_DRIVE_READONLY
    # client = Signet::OAuth2::Client.new(access_token: Token.last.token)

    # authorize = Signet::OAuth2::Client.new(
    #   :authorization_uri     => AUTHORIZATION_URI,
    #   :token_credential_uri  => TOKEN_CREDENTIAL_URI,
    #   :client_id             => ENV['GOOGLE_CLIENT_ID'],
    #   :client_secret         => ENV['GOOGLE_CLIENT_SECRET'],
    #   access_token: token.token,
    #   refresh_token: token.refresh_token,
    #   scope: scope)

    @client = Google::Apis::DriveV3::DriveService.new

    @client.authorization = token.to_authorizer(scope)

    self

  end

  def sync_files(year:Date.today.year)
    @files = []
    get_all_files(year)
    create_my_files()
    destroy_my_files()

  end 

  def create_file(my_file)
    event = Google::Apis::CalendarV3::Event.new(
      summary: my_file.title,
      description: my_file.content,
      start:{},
      end:{}
    )

    if my_file.full_day
      event.start = Google::Apis::CalendarV3::EventDateTime.new date: my_file.start.to_date.iso8601
      event.end = Google::Apis::CalendarV3::EventDateTime.new date: my_file.start.to_date.iso8601
    else
      event.start = Google::Apis::CalendarV3::EventDateTime.new date_time: my_file.start.iso8601
      event.end = Google::Apis::CalendarV3::EventDateTime.new date_time: my_file.end.iso8601
    end
 

    result = client.insert_file('primary', event)
    my_file.update ref: result.id,
      calendar_id: 'primary',
      link: result.html_link

  end

  def update_file(my_file)
    calendar_id = my_file.infos.where(ref: 'calendar_id').last.content rescue 'primary'

    event = client.get_file(calendar_id, my_file.ref)
    return unless  event
    event.summary = my_file.title
    event.description = my_file.content
    if my_file.full_day
      event.start.date = my_file.start.to_date.iso8601
      event.end.date = my_file.start.to_date.iso8601
    else
      event.start.date_time = my_file.start.iso8601
      event.end.date_time = my_file.end.iso8601
    end

    result = client.update_file(calendar_id, event.id, event)
    event
  end



  def create_my_files()
    @files.each do |event|
      Event.where(ref: event['id'], user_id:@token.user_id).first_or_create.tap do |my_file|
        # create the file
      end
    end
  end

  def destroy_my_files
    ids = @files.map{|x|x['id']}
    @token.user.items.files.where.not(ref: ids).destroy_all()
  end

  def get_all_files()
    files = self.client.list_files
    calendars.items.each do |calendar|
      get_file(year, calendar.id)
    end
  end

  def get_file(year, calendar_id )
    from = Date.new(year, 1, 1).to_time
    till = Date.new(year, 12, 31).to_time
    events = self.client.list_files(calendar_id,single_files:true, time_min: from.iso8601, time_max: till.iso8601)
    events.items.each do |event|
      p event
      @events << event.as_json.merge({calendar_id:calendar_id})
    end
  end

end

