class Apis::GoogleContact

  attr_accessor :token
  attr_accessor :list

  def initialize( token )
    
    @token = token

    @token.refresh_if_expired

    self

  end


  def sync_contacts(  )
    a = @token.to_token.get('https://www.google.com/m8/feeds/contacts/default/full/?max-results=100000&alt=json')

    data = JSON.parse(a.response.env.body)

    data["feed"]['entry'].each do |entry|
      self.get_a_contact(entry)
    end
  end

  def get_a_contact(entry)
    return if entry.nil?
    id = entry['id']['$t']
    title = entry['title']['$t']

    Contact.where(ref: id, user_id: @token.user_id).first_or_create.tap do |contact|
      contact.title = title
      contact.content = ''
      contact.save!

      # emails
      if entry['gd$email']
        entry['gd$email'].each do |item|
          email = item['address']
          contact.infos.where(ref: email).first_or_create.tap do |info|
            contact.title = email if contact.title.blank?
            info.title = 'Email'
            info.content = email
            info.save
          end
        end
        contact.save!
      end

      # # ims
      if entry['gd$im']
        entry['gd$im'].each do |item|
          im = item['address']
          contact.infos.where(ref: im).first_or_create.tap do |info|
            info.title = 'Im'
            info.content = im
            info.save
          end
        end
        contact.save!
      end

      # # addresses
      if entry['gd$postalAddress']
        entry['gd$postalAddress'].each do |item|
          address = item['$t']
          contact.infos.where(ref: address).first_or_create.tap do |info|
            info.title = 'Address'
            info.content = address
            info.save
          end
        end
        contact.save!
      end

      # # phone
      if entry['gd$phoneNumber']
        entry['gd$phoneNumber'].each do |item|
          phone = (item['uri'] || item['$t'])
          contact.infos.where(ref: phone).first_or_create.tap do |info|
            info.title = 'Phone'
            info.content = phone
            info.save
          end
        end
      end

      contact.save!

    end
    # entry.xpath('gd:im').first.attributes['address'].value
    # entry.xpath('gd:phoneNumber').first.attributes['uri'].value
  end

  
end
