class Apis::GoogleCalendar
  require 'google/apis/calendar_v3'

  # AUTHORIZATION_URI = 'https://accounts.google.com/o/oauth2/auth'
  # TOKEN_CREDENTIAL_URI = 'https://accounts.google.com/o/oauth2/token'
  attr_accessor :events
  attr_accessor :token
  attr_accessor :client

  def initialize(token)
    @token = token

    @token.refresh_if_expired
    scope = Google::Apis::CalendarV3::AUTH_CALENDAR
    # client = Signet::OAuth2::Client.new(access_token: Token.last.token)

    # authorize = Signet::OAuth2::Client.new(
    #   :authorization_uri     => AUTHORIZATION_URI,
    #   :token_credential_uri  => TOKEN_CREDENTIAL_URI,
    #   :client_id             => ENV['GOOGLE_CLIENT_ID'],
    #   :client_secret         => ENV['GOOGLE_CLIENT_SECRET'],
    #   access_token: token.token,
    #   refresh_token: token.refresh_token,
    #   scope: scope)

    @client = Google::Apis::CalendarV3::CalendarService.new

    @client.authorization = token.to_authorizer(scope)

    self

  end

  def sync_events(year:Date.today.year)
    @events = []
    get_all_events(year)
    create_my_events()
    destroy_my_events()

  end 

  def create_event(my_event)
    event = Google::Apis::CalendarV3::Event.new(
      summary: my_event.title,
      description: my_event.content,
      start:{},
      end:{}
    )

    if my_event.full_day
      event.start = Google::Apis::CalendarV3::EventDateTime.new date: my_event.start.to_date.iso8601
      event.end = Google::Apis::CalendarV3::EventDateTime.new date: my_event.start.to_date.iso8601
    else
      event.start = Google::Apis::CalendarV3::EventDateTime.new date_time: my_event.start.iso8601
      event.end = Google::Apis::CalendarV3::EventDateTime.new date_time: my_event.end.iso8601
    end
 

    result = client.insert_event('primary', event)
    my_event.update ref: result.id,
      calendar_id: 'primary',
      link: result.html_link

  end

  def update_event(my_event)
    calendar_id = my_event.infos.where(ref: 'calendar_id').last.content rescue 'primary'

    event = client.get_event(calendar_id, my_event.ref)
    return unless  event
    event.summary = my_event.title
    event.description = my_event.content
    if my_event.full_day
      event.start.date = my_event.start.to_date.iso8601
      event.end.date = my_event.start.to_date.iso8601
    else
      event.start.date_time = my_event.start.iso8601
      event.end.date_time = my_event.end.iso8601
    end

    result = client.update_event(calendar_id, event.id, event)
    event
  end



  def create_my_events()
    @events.each do |event|
      Event.where(ref: event['id'], user_id:@token.user_id).first_or_create.tap do |my_event|
        my_event.start = (event['start']['date'] || event['start']['date_time']).to_time 
        my_event.end = (event['end']['date'] || event['end']['date_time']).to_time 
        my_event.content = event['description']
        my_event.title = event['summary']
        my_event.save
        
        my_event.calendar_id = event['calendar_id']
        my_event.link = event['html_link']

      end
    end
  end

  def destroy_my_events
    ids = @events.map{|x|x['id']}
    @token.user.items.events.where.not(ref: ids).destroy_all()
  end

  def get_all_events(year)
    calendars = self.client.list_calendar_lists
    calendars.items.each do |calendar|
      get_event(year, calendar.id)
    end
  end

  def get_event(year, calendar_id )
    from = Date.new(year, 1, 1).to_time
    till = Date.new(year, 12, 31).to_time
    events = self.client.list_events(calendar_id,single_events:true, time_min: from.iso8601, time_max: till.iso8601)
    events.items.each do |event|
      p event
      @events << event.as_json.merge({calendar_id:calendar_id})
    end
  end

end

