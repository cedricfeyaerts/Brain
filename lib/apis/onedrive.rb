class Apis::Onedrive
  require 'onedrive'

  # def self.token
  #   unless @_token
  #     @_token = Token.provider('google_oauth2').last.to_token
  #   end
  #   @_token 
  # end

  attr_accessor :files_attributes
  attr_accessor :token
  attr_accessor :client

  def initialize( tken )
    
    @token = tken

    @token.refresh_if_expired

    @client = Onedrive::Client.new token: token.token
    self
  end

  def sync_documents
    @files_attributes = []
    self.parse_drive @client.drives.first.children[6]
    @token.user.onedrive_folders.each do |id|
      item = @client.item(id)
      next if item.nil?
      self.parse_drive @client.item(id)
    end
    self.create_documents
    self.destroy_documents

  end

  def folder_list
    @client.drives.first.children.select{|x|x.attributes.key?('folder')}.map{|x|{id: x.id, name: x.name, attributes:x.attributes}}
  end

  # protected

  def create_documents
    @files_attributes.each do |attributes|
      Document.where(ref: attributes['id']).first_or_create.tap do |document|
        document.update title: attributes['name'], 
          link: attributes['webUrl'], 
          path: URI.decode(attributes['parentReference']['path'].gsub('/drive/root:', '')),
          url: attributes['@content.downloadUrl'],
          user_id: @token.user_id
      end
    end
  end

  def destroy_documents
    ids = @files_attributes.map{|x|x['id']}
    @token.user.items.documents.where.not(ref: ids).destroy_all()
  end

  def parse_drive(item)
    if item.attributes['file']
      @files_attributes << item.attributes
      return
    else
      children = item.children
      if children.empty? 
        return
      else
        children.each do |child|
          parse_drive child
        end
      end
    end

  end

  
end
